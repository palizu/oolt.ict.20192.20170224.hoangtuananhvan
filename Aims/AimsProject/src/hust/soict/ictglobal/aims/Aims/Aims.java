package hust.soict.ictglobal.aims.Aims;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.order.Order.Order;
import hust.soict.ictglobal.aims.media.*;
import java.util.*;

public class Aims {
	
	//private static List<Media> itemList = new ArrayList<Media>();
	private static Collection itemList = new ArrayList();
	private static List<Order> orders = new ArrayList<Order>();
	
	private static void createOrder() {
		Order order = new Order();
		String id = "order" + Integer.toString(orders.size());
		order.setId(id);
		orders.add(order);
		System.out.println("Order id " + id + " is created !!");
	}
	/*
	private static void add(Order order) {
		System.out.println("Enter type of media: \n1. Book\n2. Digital Video Disc\n3. Compact Disc\nEnter choice: ");
		Scanner kb = new Scanner(System.in);
		String title = new String();
		int type = Integer.parseInt(kb.nextLine());
		switch(type) {
			case 1:
				System.out.println("Enter book title: ");
				title = kb.nextLine();
				for (Media item: itemList) {
					if (item.getTitle().equals(title)) {
						order.addMedia(item);
						System.out.println("Book title " + title + " is added") ;
					}
				}
				System.out.println("Book title " + title + " is not in the list") ;
				break;
			case 2:
				System.out.println("Enter disc title: ");
				title = kb.nextLine();
				for (Media item: itemList) {
					if (item.getTitle().equals(title)) {
						order.addMedia(item);
						System.out.println("Disc title " + title + " is added") ;
						return;
					}
				}
				System.out.println("Disc title " + title + " is not in the list") ;
				break;
			case 3:
				System.out.println("Enter disc title: ");
				title = kb.nextLine();
				System.out.println("Enter artist: ");
				String artist = kb.nextLine();
				int length = 0;
				
				ArrayList<Track> tracks = new ArrayList<Track>();
				System.out.println("Enter number of tracks ($3.99 each track): ");
				int n = Integer.parseInt(kb.nextLine());
				for (int i = 0; i < n; i ++) {
					System.out.println("Enter track number " + (i + 1) + " title: ");
					String trackTitle = kb.nextLine();

					System.out.print("Enter track's length: ");
					length = Integer.parseInt(kb.nextLine());
					tracks.add(new Track(trackTitle, length));
				}
				CompactDisc disc = new CompactDisc(title, artist, length, tracks) ;
				disc.setCost(3.99f * tracks.size());
				order.addMedia(disc);
				System.out.println("Disc title " + title + " is added") ;
				
				playDiscOption(disc);
				break;
			default: 
				System.out.println("Invalid choice");
				break;
		}
	}
	
	private static void playDiscOption(CompactDisc disc) {
		System.out.println("Play the selected disc ??\n1. yes\n2. no") ;
		Scanner kb = new Scanner(System.in);
		int choice = Integer.parseInt(kb.nextLine());
		if (choice == 1) disc.play();
		kb.close();
	}
	
	private static void addToOrder() {
		String orderId = new String();
		String itemId = new String();
		Scanner kb = new Scanner(System.in);
		
		System.out.println("Enter id of the order: ");
		orderId = kb.nextLine().strip();
		for (Order _order: orders) {
			if (_order.getId().equals(orderId)) {
				add(_order);
				return;
			}
		}
		System.out.println("Order " + orderId + " is not created");
	}
	*/
	private static void addItem() {
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		dvd1.setId("dvd1");
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		dvd2.setId("dvd2");

		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladins");
		dvd3.setCategory("Animation");
		dvd3.setCost(21.95f);
		dvd3.setDirector("Director 3");
		dvd3.setLength(100);
		dvd3.setId("dvd3");
		
		Book book1 = new Book("Harry Potter 1");
		book1.addAuthor("J.K Rowling");
		book1.setCategory("novel");
		book1.setCost(4.25f);
		book1.setId("book1");
		
		Book book2 = new Book("The godfather");
		book2.addAuthor("Mario Puzo");
		book2.setCategory("crime novel");
		book2.setCost(5.25f);
		book2.setId("book2");
		
		itemList.add(dvd1);
		itemList.add(dvd2);
		itemList.add(dvd3);
		//itemList.add(book1);
		//itemList.add(book2);
	}

	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	
	public static void main(String args[]) {
		
		
		addItem();
		// unsorted order
		Iterator iter = itemList.iterator();
		System.out.println("----------------------------------------") ;
		System.out.println("The DVDs currently in order are: "); 
		
		while (iter.hasNext()) 
				System.out.println(((DigitalVideoDisc) iter.next()).getTitle());
		
		// sort the order based on compareTo
		// method
		Collections.sort((List) itemList);
		iter = itemList.iterator();
		System.out.println("----------------------------------------") ;
		System.out.println("The DVDs in sorted order are: "); 
		while (iter.hasNext()) 
			System.out.println(((DigitalVideoDisc) iter.next()).getTitle());
		System.out.println("----------------------------------------") ;
		/*
		Thread thread = new Thread(new MemoryDeamon());
		thread.setDaemon(true);
		thread.start();
		
		int task;
		addItem();
		String orderId = new String();
		String itemId = new String();
		
		while (true) {
			showMenu();
			Order order = new Order(); 
			Scanner kb = new Scanner(System.in);
			task = Integer.parseInt(kb.nextLine());
			
			switch(task) {
				case 1:
					createOrder();
					break;
					
				case 2:
					addToOrder();
					break;
					
				case 3:
					System.out.println("Enter id of the order: ");
					orderId = kb.nextLine().strip();
					for (Order _order: orders) {
						if (_order.getId().equals(orderId)) {
							System.out.println("Enter id of the item: ");
							itemId = kb.nextLine().strip();
							for (Media item: itemList) {
								if (itemId.equals(item.getId())) {
									_order.removeMedia(item);
									System.out.println("Item " + itemId + " is deleted");
									break;
								}
							}
							break;
						}
					}
					break;
					
				case 4:
					System.out.println("Enter id of the order: ");
					orderId = kb.nextLine().strip();
					for (Order _order: orders) {
						if (_order.getId().equals(orderId)) {
							int i = 1;
							System.out.println("Order " + orderId + " info: ");
							for (Media item: _order.itemsOrdered) {
								System.out.print(i + ". " + "Item id: " + item.getId() + ", title: " + item.getTitle());
								System.out.println(" , cost: " + item.getCost());
							}
							System.out.println("TOTAL COST: ");
							System.out.printf("%.2f\n", _order.totalCost());
							break;
						}
					}
					break;
				case 0:
					return;
				default:
					System.out.println("Invalid choice");
					break;
			}
		}*/
	}		
}