package hust.soict.ictglobal.aims.Aims;
import hust.soict.ictglobal.aims.media.*;

public class BookTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Book book = new Book("Harry Potter 1");
		book.addAuthor("J.K Rowling");
		book.setCategory("novel");
		book.setCost(4.25f);
		book.setId("book1");
		book.setContent("aa bb cc dd");
		System.out.print(book.toString());
	}

}
