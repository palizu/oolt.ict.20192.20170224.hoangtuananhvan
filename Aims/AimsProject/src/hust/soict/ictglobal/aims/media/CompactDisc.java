package hust.soict.ictglobal.aims.media;
import java.util.*;

import hust.soict.ictglobal.aims.Aims.PlayerException;

public class CompactDisc extends Disc implements Playable, Comparable{
	// fields
	private String artist;
	private int length;
	private ArrayList<Track> tracks = new ArrayList<Track>();
	
	// constructor
	public CompactDisc(String title, String artist, int length, ArrayList tracks) {
		super(title);
		this.artist = artist;
		this.length = length;
		this.tracks = tracks;
	}
	public CompactDisc(String title) {
		super(title);
	}
	
	//getter and setter
	public int getNumberOfTrack() {
		return tracks.size();
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	// add and remove track
	public void addTrack(Track track) {
		if (tracks.contains(track))
			System.out.println("Track " + track.getTitle() + " is already existed");
		else {
			tracks.add(track);
			System.out.println("Track " + track.getTitle() + " is added");
		}
	}
	public void removeTrack(Track track) {
		if (tracks.contains(track)) {
			tracks.remove(track);
			System.out.println("Track " + track.getTitle() + " is removed from the list");
		} else System.out.println("Track " + track.getTitle() + " is not in the list");
	}
	
	//get length of all track in the cd
	@Override
	public int getLength() {
		length = 0;
		for (Track track: tracks) 
			length += track.getLength();
		return length;
	}
	
	@Override
	public void play() throws PlayerException {
		if (this.getLength() <= 0) {
			System.err.println("ERROR: CD length is 0");
			throw (new PlayerException());
		}
		
		System.out.println("Playing CD: " + this.getTitle());
		System.out.println("CD length: " + this.getLength());
		
		Iterator<Track> iter = tracks.iterator();
		Track nextTrack;
		while (iter.hasNext()) {
			nextTrack = (Track) iter.next();
			try { 
				nextTrack.play();
			} catch (PlayerException e) {
				e.printStackTrace();
			}
		}
	}
	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		CompactDisc disc = (CompactDisc) obj;
		//return this.getTitle().compareTo(disc.getTitle());
		if (this.getNumberOfTrack() == disc.getNumberOfTrack()) {
			if (this.getLength() == disc.getLength()) return 0;
			if (this.getLength() < disc.getLength()) return -1;
			return 1;
		}
		if (this.getNumberOfTrack() < disc.getNumberOfTrack()) return -1;
		return 1;
	}
}
