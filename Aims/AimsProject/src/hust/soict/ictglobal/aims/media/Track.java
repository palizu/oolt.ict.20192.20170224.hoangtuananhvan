package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.Aims.PlayerException;

public class Track implements Playable, Comparable{
	private String title;
	private int length;
	
	// constructor
	public Track(String title, int length) {
		this.title = title;
		this.length = length;
	}
	
	// getter and setter
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	
	@Override
	public void play() throws PlayerException{
		// TODO Auto-generated method stub
		if (this.getLength() <= 0) {
			System.err.println("ERROR: DVD length is 0");
			throw (new PlayerException()) ;
		}
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}

	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		Track track = (Track) obj;
		return this.getTitle().compareTo(track.getTitle());
	}
}
