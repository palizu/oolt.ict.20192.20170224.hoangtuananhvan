package hust.soict.ictglobal.aims.media;


public abstract class Media {

	private String title;
	private String category;
	private float cost;
	private String id;
	
	
	public String getTitle() {
		return title;
	}	
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	// constructor
	public Media(String title) {
		// TODO Auto-generated constructor stub
		this.title = title;
	}
	
	public Media(String title, String category) {
		this(title);
		this.category = category;
	}
	
	public int compareTo(Object obj) throws NullPointerException, ClassCastException {
		try {
			if(obj instanceof Media) {
				if(this.equals(obj)) return 0;
				else return 1;
			}
			return 1;
		}
		catch(NullPointerException e) {
			e.printStackTrace();
		}
		catch(ClassCastException e) {
			e.printStackTrace();
		}
		return 1;
		
	}
	
	public boolean equals(Media obj) {
		if(this.title.equals(obj.getTitle()) && this.cost == obj.getCost()) return true;
		return false;
	}
}
