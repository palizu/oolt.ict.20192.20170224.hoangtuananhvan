package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.Aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable, Comparable{
	//fields
	private	String director;
	private int length;
	
	// constructor
	public DigitalVideoDisc(String title) {
		super(title);
	}
	public DigitalVideoDisc(String title, String category, String director, int length) {
		super(title, category);
		this.director = director;
		this.length = length;
	}
	
	boolean search(String title) {
		String[] token = title.split(" ");
		String discTitle = this.getTitle();
		for (int i = 0; i < token.length; i++) {
			if (discTitle.contains(token[i])) 
				return true;
		}
		return false;
 	}
	
	@Override
	public void play() throws PlayerException{
		// TODO Auto-generated method stub
		if (this.getLength() <= 0) {
			System.err.println("ERROR: DVD length is 0");
			throw (new PlayerException()) ;
		}
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		DigitalVideoDisc disc = (DigitalVideoDisc) obj;
		//return this.getTitle().compareTo(disc.getTitle());
		if (this.getCost() == disc.getCost()) return 0;
		if (this.getCost() < disc.getCost()) return -1;
		return 1;
	}
}

