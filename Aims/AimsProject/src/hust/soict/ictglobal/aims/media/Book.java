package hust.soict.ictglobal.aims.media;
import java.util.ArrayList;
import java.util.*;

public class Book extends Media implements Comparable {
	// field
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> tokenContent;
	private Map<String, Integer> wordFreq ;

	//setter and getter
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		processContent();
	}
	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	//constructor
	public Book(String title) {
		// TODO Auto-generated constructor stub
		super(title);
	}
	
	public Book(String title, String category) {
		super(title, category);
	}
	
	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors; 
		//TODO: check author condition
	}

	// method
	public void addAuthor(String authorName) {
		if (!authors.contains(authorName)) {
			authors.add(authorName);
			System.out.println(authorName + " is added to the list");
		}
		else System.out.println(authorName + " is not in the list");
	}
	
	public void removeAuthor(String authorName) {
		if (authors.contains(authorName)) {
			authors.remove(authorName);
			System.out.println(authorName + " is removed from the list");
		}
		else System.out.println(authorName + " is not in the list");
	}
	
	public void processContent() {
		String[] token = content.replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase().split(" ");
		tokenContent = new ArrayList<String>();
		wordFreq = new HashMap<String, Integer>();
		for (String _token: token) {
			if (!tokenContent.contains(_token)) {
				tokenContent.add(_token) ;
				wordFreq.put(_token, 1);
			} else wordFreq.replace(_token, wordFreq.get(_token) + 1);
		}
		//tokenContent.sort(null);
	}
	
	@Override
	public String toString() {
		String res = "" ;
		res += content;
		res += "\nNumber of token: " ;
		res += Integer.toString(tokenContent.size());
		res += "\nWord frequency: \n";
		for (String _token: tokenContent) {
			res += _token;
			res += ": ";
			res += Integer.toString(wordFreq.get(_token));
			res += "\n";
		}
		return res;
	}
	
	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		Book book = (Book) obj;
		return this.getTitle().compareTo(book.getTitle());
	}

}
