package hust.soict.ictglobal.aims.media;

public class Disc extends Media{
	//constructor
	public Disc(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}
	public Disc(String title) {
		super(title);
	}
	
	//field
	private int length;
	private String director;
	 
	//setter and getter
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	
	
}
