package hust.soict.ictglobal.aims.order.Order;
import java.util.ArrayList;

//import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
//import hust.soict.ictglobal.date.MyDate;
import hust.soict.ictglobal.aims.media.Media;


public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	//private MyDate dateOrdered;
	public static final int MAX_LIMITTED_ORDERS = 5;
	//private static int nbOrders = 0;
	
	public ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void addMedia(Media item) {
		if (itemsOrdered.size() < MAX_NUMBER_ORDERED) {
			itemsOrdered.add(item);
			System.out.println(item.getTitle() + " is added!!");
		} else System.out.println("The order list is full!!");
	}
	
	public void removeMedia(Media item) {
		if (itemsOrdered.isEmpty()) {
			System.out.println("The order list is empty!!");
			return;
		}
		if (itemsOrdered.contains(item)) {
			itemsOrdered.remove(item);
			System.out.println(item.getTitle() + " is removed!!");
		}
	}
	
	public double totalCost() {
		double res = 0;
		for (Media item: itemsOrdered) res += item.getCost();
		return res;
	}
	/*
	public Order() {
		if (nbOrders == MAX_LIMITTED_ORDERS) {
			System.out.println("Order list is full");
			return;
		}
		nbOrders += 1;
		this.dateOrdered = new MyDate();
		this.qtyOrdered = 0;
	}

	public MyDate getDateOrdered() {
		return dateOrdered;
	}
	
	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	
	public float totalCost() {
		float res = 0;
		return res ;
	}
	
	public void printOrder() {
		System.out.println("*********************Order************************");
		System.out.print("Date: ");
		dateOrdered.print();
		for (int i = 0; i < qtyOrdered; i++) {
			System.out.print(i + 1 + ". " + itemOrdered[i].getTitle() + " - " + itemOrdered[i].getCategory());
			System.out.print(" - " + itemOrdered[i].getDirector() + " - " + itemOrdered[i].getLength());
			System.out.println(": " + itemOrdered[i].getCost() + "$");
			if (itemOrdered[i].getCost() == 0) System.out.println("This is a lucky item\n");
		}
		System.out.println("Total Cost: " + totalCost());
	}
	
	public DigitalVideoDisc getALuckyItem() {
		int luckyItem = (int)(Math.random() * qtyOrdered);
		itemOrdered[luckyItem].setCost(0);
		return itemOrdered[luckyItem];
	}
	*/
}

