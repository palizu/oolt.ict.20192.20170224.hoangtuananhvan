package hust.soict.ictglobal.date;
import java.util.List;
import java.util.ArrayList;
import java.time.LocalDate;
public class DateTesting {
	public static void main(String[] agrs) {
		List<LocalDate> dateList= new ArrayList<>() ;
		dateList.add(LocalDate.of(1999, 8, 26));
		dateList.add(LocalDate.of(1999, 8, 25));
		dateList.add(LocalDate.now());
		
		dateList = DateUtils.sortDates(dateList);
		dateList.forEach(System.out::println);
	}
}