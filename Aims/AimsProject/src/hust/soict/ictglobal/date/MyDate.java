package hust.soict.ictglobal.date;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Scanner;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

public class MyDate {
	private int day, month, year;

	public MyDate() {
		Calendar date = Calendar.getInstance();
		this.day = date.get(date.DATE);
		this.month = date.get(date.MONTH);
		this.year = date.get(date.YEAR);
	}
		
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}	
	
	private LocalDate convertToLocalDate(String dateStr) {
		SimpleDateFormat ft = new SimpleDateFormat("MMMM dd yyyy") ;
		Date date = new Date();
		try {
			date = ft.parse(dateStr);
			return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		} catch(ParseException e) {
			return null;
		}
	}
	
	public MyDate(String dateStr) {
		LocalDate date = convertToLocalDate(dateStr);
		this.day = date.getDayOfMonth();
		this.month = date.getMonthValue();
		this.year = date.getYear();
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day > 0 && day < 32) 
			this.day = day;
		else System.out.print("INVALID DAY\n");
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (month > 0 && month <= 12) 
			this.month = month;
		else System.out.print("INVALID MONTH\n");
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		if (year > 0) 
			this.year = year;
		else System.out.print("INVALID YEAR \n");
	}
	
	public void accept() {
		
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter date: ");
		String dateStr = keyboard.nextLine();
		
		LocalDate date = convertToLocalDate(dateStr);
		this.day = date.getDayOfMonth();
		this.month = date.getMonthValue();
		this.year = date.getYear();
	}
	
	public void print() {
		System.out.print(day + "/" + month + "/" + year + '\n');
	}
}
