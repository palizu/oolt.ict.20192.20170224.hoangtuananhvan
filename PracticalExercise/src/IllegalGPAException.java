
public class IllegalGPAException extends Exception {
	public IllegalGPAException(String msg) {
		super(msg);
	}
}
