import java.util.Scanner;
import java.text.SimpleDateFormat;
import java.util.Date;

public class main {

	public static void main(String[] args) throws IllegalBirthDayException, IllegalGPAException, Exception{
		// TODO Auto-generated method stub
		Scanner kb = new Scanner(System.in);
		
		System.out.println("Enter StudentID: ");
		String studentID = kb.nextLine();
		
		System.out.println("Enter Student name: ");
		String name = kb.nextLine();
		
		System.out.println("Enter birthday: ");
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
		String bd = kb.nextLine();
		Date birthday = new Date();
		try {
			birthday = formatter.parse(bd);
		} catch (Exception e) {
			throw (new IllegalBirthDayException("wrong date format"));
		}
		
		
		System.out.println("Enter GPA: ");
		float gpa = kb.nextFloat();
		if (gpa < 0 || gpa > 10) {
			throw  (new IllegalGPAException("Value of gpa is not valid")) ;
		}
		System.out.println("ID: " + studentID);
		System.out.println("Name: " + name);
		System.out.println("DOB: " + formatter.format(birthday));
		System.out.println("GPA: " + gpa);
		
	}

}
