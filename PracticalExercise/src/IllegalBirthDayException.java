
public class IllegalBirthDayException extends Exception {
	public IllegalBirthDayException(String msg) {
		super(msg);
	}
}
