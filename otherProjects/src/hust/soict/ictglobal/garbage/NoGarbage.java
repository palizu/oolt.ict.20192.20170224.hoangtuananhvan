package hust.soict.ictglobal.garbage;

import java.io.*;
import java.util.Scanner;

public class NoGarbage{
	public static void main(String[] args) throws IOException  {
		File f = new File("/Users/hoangvan/Desktop/Work/OOP_lab/oolt.ict.20192.20170224.hoangtuananhvan/otherProjects/src/hust/soict/ictglobal/garbage/RandomString.txt");
		if (!f.exists()) {
			System.out.print("Can't read file");
			return;
		}
		BufferedReader br = new BufferedReader(new FileReader(f));
		String s = "";
		StringBuffer sb = new StringBuffer();
		String strLine;
		int prevAddr = s.hashCode();
		int cnt = 0;
		
		// Read without StringBuffer 
		while ((strLine = br.readLine()) != null) {
			sb.append(strLine);
			
			int curAddr = s.hashCode();
			if (prevAddr != curAddr) {
				prevAddr = curAddr;
				System.out.println(curAddr);
				cnt++;
			}
		}
		
		System.out.print("Garbage: " + cnt);
		
		br.close();
	}
}
