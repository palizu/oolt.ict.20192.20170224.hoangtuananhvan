package hust.soict.ictglobal.lab02;
import javax.swing.JOptionPane;
public class ChoosingOption {
    public static void main(final String[] args) {
        final int option = JOptionPane.showConfirmDialog(null, "Do you want to change the first class ticker");
        JOptionPane.showMessageDialog(null, "You've chosen: " + (option== JOptionPane.YES_NO_OPTION ? "YES" : "NO"));
        System.exit(0);
    }
}