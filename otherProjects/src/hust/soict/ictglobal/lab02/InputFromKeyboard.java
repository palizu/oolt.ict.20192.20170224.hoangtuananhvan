package hust.soict.ictglobal.lab02;
import java.util.Scanner;
public class InputFromKeyboard {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in) ;

        System.out.println("Name ?");
        String strName = keyboard.nextLine();
        System.out.println("Age ?");
        int age = keyboard.nextInt();
        System.out.print("Height ?");
        double h = keyboard.nextDouble();

        System.out.println("Mr./Mrs. " + strName + ", " + age + "years old " + h + "cm tall  !!") ; 
    }
}