package hust.soict.ictglobal.lab02;
import java.util.Scanner;
public class ex4 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in) ;

        System.out.println("Enter n: "); 
        int n = keyboard.nextInt() ;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - i - 1; j ++) 
                System.out.print(" ") ;
            for (int j = 0; j < 2 * i + 1; j++) 
                System.out.print("*") ;
            System.out.println();
        }
    }
}