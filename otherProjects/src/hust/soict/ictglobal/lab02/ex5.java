package hust.soict.ictglobal.lab02;
import java.util.Scanner;
import java.util.Arrays; 
import java.util.List;
public class ex5 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Enter month (mm/yyyy): ");
        String month = keyboard.nextLine();
        boolean flag = false ;
        String[] info = month.split("/");
        int m = Integer.parseInt(info[0]);
        int y = Integer.parseInt(info[1]);
        int res = 0;

        if ((y % 400 == 0) || (y % 4 == 0 && y % 100 != 0)) {
            flag = true ;
        }
        
        if (m == 2) {
            if (flag) res = 29 ;
            else res = 28 ;
        } else if (m <= 7) {
            if (m %2 == 1) res = 31 ;
            else res = 30 ;
        } else if (m% 2 == 0) res = 30 ;
        else res = 31 ;

        System.out.println("Number of days is: " + res) ;
    }
}