package hust.soict.ictglobal.lab02;
import java.util.Scanner;
import java.util.Arrays;
public class ex7{
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Enter size of the two matrices (mxn): ");
        String strSize = keyboard.nextLine();
        String[] splitedSize = strSize.split("x");
        int m = Integer.parseInt(splitedSize[0]);
        int n = Integer.parseInt(splitedSize[1]);

        int a[][] = new int[m][n];

        System.out.println("Enter elements in the first matrix: ");
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                a[i][j] = keyboard.nextInt();

        System.out.println("Enter elements in the second matrix: ");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int temp = keyboard.nextInt();
                a[i][j] += temp;
            }
        }

        System.out.println("Added matrix: ");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println(" ");
        }
    }
}