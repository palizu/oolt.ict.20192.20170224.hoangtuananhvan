package hust.soict.ictglobal.lab02;
import java.util.*;
public class ex6{
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("Enter number of elements: ");
        int n = keyboard.nextInt();

        int a[] = new int[n];
        System.out.print("Enter all elements of array: ");
        for (int i = 0; i < n; i++) {
            a[i] = keyboard.nextInt() ;
        }

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) 
                if (a[i] > a[j]) {
                    int temp = a[i] ;
                    a[i] = a[j] ;
                    a[j] = temp ;
                }
        }

        System.out.println("Sorted array: " + Arrays.toString(a));
    }
}