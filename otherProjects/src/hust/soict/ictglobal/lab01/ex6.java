package hust.soict.ictglobal.lab01;
import javax.swing.JOptionPane;
import java.lang.Math ;
public class ex6 {

    private static void oneVarFirstDegreeEquation() {
        String title = "ax + b = 0" ;
        String strA, strB ;
        double a, b;
        String res = "" ;

        strA = JOptionPane.showInputDialog(null, "Enter a: ", "Enter coefficient " + title, JOptionPane.INFORMATION_MESSAGE) ;
        a = Double.parseDouble(strA) ;
        strB = JOptionPane.showInputDialog(null, "Enter b: ", "Enter coefficient " + title, JOptionPane.INFORMATION_MESSAGE) ;
        b = Double.parseDouble(strB) ;
            
        if (a == 0 && b == 0) {
            res = "Infinite solutions !!" ;
        }
        if (a == 0 && b != 0) {
            res = "No solution !!" ;
        }
        res = "The solution is: " + (-a / b) ;
        JOptionPane.showMessageDialog(null, res, "The solution of " + title, JOptionPane.INFORMATION_MESSAGE) ;
    }

    private static void twoVarFirstDegreeEquation() {
        String title = "ax + by = c" ;
        String strA, strB, strC ;
        double a, b, c;
        String res = "" ;

        strA = JOptionPane.showInputDialog(null, "Enter a: ", "Enter coefficient " + title, JOptionPane.INFORMATION_MESSAGE) ;
        a = Double.parseDouble(strA) ;
        strB = JOptionPane.showInputDialog(null, "Enter b: ", "Enter coefficient " + title, JOptionPane.INFORMATION_MESSAGE) ;
        b = Double.parseDouble(strB) ;
        strC = JOptionPane.showInputDialog(null, "Enter c: ", "Enter coefficient " + title, JOptionPane.INFORMATION_MESSAGE) ;
        c = Double.parseDouble(strC) ;
            
        if (a == 0 && b == 0 && c == 0) {
            res = "Infinite solutions !!" ;
        } else if (a == 0 && b == 0 && c != 0) {
            res = "No solution !!" ;
        } else if (a == 0 && b != 0) {
            res = "y = " + (c / b) + "with any x" ;
        } else if (a != 0 && b == 0) {
            res = "x = " + (c / a) + "with any y" ;
        } else 
            res = "x = (" + c + "- " + b + "y) / " + a + " with any y" ;
        JOptionPane.showMessageDialog(null, res, "The solution of " + title, JOptionPane.INFORMATION_MESSAGE) ;
    }

    private static void oneVarSecondDegreeEquation() {
        String title = "ax^2 + bx + c = 0" ;
        String strA, strB, strC;
        double a, b, c;
        String res = "" ;

        strA = JOptionPane.showInputDialog(null, "Enter a: ", "Enter coefficient " + title, JOptionPane.INFORMATION_MESSAGE) ;
        a = Double.parseDouble(strA) ;
        strB = JOptionPane.showInputDialog(null, "Enter b: ", "Enter coefficient " + title, JOptionPane.INFORMATION_MESSAGE) ;
        b = Double.parseDouble(strB) ;
        strC = JOptionPane.showInputDialog(null, "Enter c: ", "Enter coefficient " + title, JOptionPane.INFORMATION_MESSAGE) ;
        c = Double.parseDouble(strC) ; 

        double delta = b*b - 4*a*c ;
        if (delta < 0) 
            res =  "No solution !!" ;
        if (delta == 0) 
            res =  "Solution: " + (-b / 2 / a) ;
        delta = Math.sqrt(delta) ;
        res = "Solution :\n" + ((delta - b) / 2 / a) + "\n" + ((-delta - b) / 2 / a) ;
        JOptionPane.showMessageDialog(null, res, "The solution of " + title, JOptionPane.INFORMATION_MESSAGE) ;
    }

    public static void main(String args[]) {
        String task ;
        String taskList = "1. First degree equation with one variable\n 2. First degree equation with two variables\n 3. Second degree equation with one variable\n\nEnter task:" ;
        while (true) {
            task = JOptionPane.showInputDialog(null, taskList, "Choose task", JOptionPane.INFORMATION_MESSAGE) ;
            if (task.equals("1")) 
                oneVarFirstDegreeEquation() ;
            else if (task.equals("2")) 
                twoVarFirstDegreeEquation();
            if (task.equals("3")) 
                oneVarSecondDegreeEquation() ;
        }
    }
}