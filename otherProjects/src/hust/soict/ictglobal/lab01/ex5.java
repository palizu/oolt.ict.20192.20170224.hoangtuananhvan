package hust.soict.ictglobal.lab01;
import javax.swing.JOptionPane;
public class ex5 {
    public static void main(String args[]) {
        String strNum1, strNum2, res ;
        double num1, num2 ;

        strNum1 = JOptionPane.showInputDialog(null, "Enter num1", JOptionPane.INFORMATION_MESSAGE) ;
        num1 = Double.parseDouble(strNum1) ;

        strNum2 = JOptionPane.showInputDialog(null, "Enter num2", JOptionPane.INFORMATION_MESSAGE) ;
        num2 = Double.parseDouble(strNum2) ;

        double sum, diff, product, quotient;
        
        sum = num1 + num2 ;
        diff = num1 - num2 ;
        product = num1 * num2 ;

        res = "Sum: " + sum + "\nDifference: " + diff + "\nProduct: " + product ;
        if (num2 == 0) {
            res += "\nNo quotient because divisor is 0" ;
        } else {
            quotient = num1 / num2 ;
            res += "\nQuotient: " + quotient ;
        }
        JOptionPane.showMessageDialog(null, res, "Result" ,JOptionPane.INFORMATION_MESSAGE);
        System.exit(0) ;
    }
}